<?php

    require_once("autoload.php");

    class User extends Connection{
        private $name;
        private $phone_num;
        private $email;
        private $conn;

        public function __construct()
        {
           $this->conn = new Connection();
           $this->conn = $this->conn->connection();
        }
        public function insertUser(string $strName, string $strPhone, string $strEmail)
        {
            $this->name = $strName;
            $this->phone_num = $strPhone;
            $this->email = $strEmail;

            $sql = "INSERT INTO user(name,phone_num,email) VALUES(?,?,?)";
            $insert = $this->conn->prepare($sql);
            $arrData = array($this->name, $this->phone_num, $this->email);
            $resInsert = $insert->execute($arrData);
            $lastId = $this->conn->lastInsertId();
            return $lastId;
        }

        public function updateUser(int $id, string $strName, string $strPhone, string $strEmail)
        {
            $this->name = $strName;
            $this->phone_num = $strPhone;
            $this->email = $strEmail;

            $sql = "update user set name=?, phone_num=?, email=? where=$id";

            $update = $this->conn->prepare($sql);

            $arrData = array($this->name,$this->phone_num, $this->email);

            $resUpdate = $update->execute($arraData);

            return $resUpdate;
        }

        public function getUpdate(int $id)
        {
            $sql = "SELECT * FROM user WHERE id = ? ";

            $query = $this->conn->prepare($sql);

            $arrWhere = array($id);

            $query->execute($arrWhere);

            $request = $query->fetch(PDO::FETCH_ASSOC);

            return $request;
        }

        public function deletUser(int $id)
        {
            $sql = "DELETE FROM user WHERE id = ? ";

            $query = $this->conn->prepare($sql);

            $arrWhere = array($id);

            $del = $delete->execute($arrWhere);

            return $del;
        }

        public function getUser()
        {
            $sql = "SELECT * FROM user";
            $get = $this->conn->query($sql);
            $resGet = $get->fetchall(PDO::FETCH_ASSOC);
            return $resGet;
        }
    }
